import { useCallback, useReducer } from 'react';
import { HTTP_ACTIONS, hyperTextTransferProtocolReducer } from '../helpers';
import { HttpReducerFn } from '../helpers/reducers';

export interface SendRequestParams<B = any, E = any, BR = any> {
  url: string;
  method: string;
  headers?: { [key: string]: string };
  body?: B;
  onSuccess?: (data: BR) => void;
  onError?: (error: any) => void;
  extra?: E;
}
type SendRequestFn = (params: SendRequestParams) => void;

const useHyperTextTransferProtocol = <D = any, E = any>() => {
  const [hyperTextTransferProtocolState, dispatch] = useReducer<HttpReducerFn<D, E>>(
    hyperTextTransferProtocolReducer,
    {
      loading: false,
      error: null,
      data: null,
      extra: null,
    }
  );
  const { loading: isLoading, error, data, extra } = hyperTextTransferProtocolState;

  const clear = useCallback(() => {
    dispatch({
      type: HTTP_ACTIONS.CLEAR,
    });
  }, []);

  const sendRequest: SendRequestFn = useCallback((params) => {
    const { url, method, headers, body, onSuccess, onError, extra } = params;

    dispatch({
      type: HTTP_ACTIONS.SEND_REQUEST,
    });
    const requestOptions: RequestInit = {
      method: method ? method : 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    };

    if (headers) {
      requestOptions.headers = {
        ...requestOptions.headers,
        ...headers,
      };
    }

    if (body) {
      requestOptions.body = JSON.stringify(body);
    }

    fetch(url, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: HTTP_ACTIONS.RESPONSE,
          data,
          extra,
        });

        if (onSuccess) {
          onSuccess(data);
        }
      })
      .catch((error) => {
        dispatch({
          type: HTTP_ACTIONS.ERROR,
          message: 'Something unexpected (wrong) occurred!',
        });

        if (onError) {
          onError(error);
        }
      });
  }, []);

  return {
    isLoading,
    error,
    data,
    extra,
    clear,
    sendRequest,
  };
};

export default useHyperTextTransferProtocol;
