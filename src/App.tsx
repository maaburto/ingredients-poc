import { FC, ReactNode, useContext } from 'react';

import { AuthSignIn } from './components/AuthSignIn';
import Ingredients from './components/Ingredients/Ingredients';
import { AuthContext } from './store';
import './App.css';

interface AppLayoutProps {
  children: ReactNode | JSX.Element | JSX.Element[];
}

const AppLayout: FC<AppLayoutProps> = (props) => <div className='App'>{props.children}</div>;

const App = () => {
  const authContext = useContext(AuthContext);

  if (authContext.isAuth) {
    return (
      <AppLayout>
        <Ingredients />
      </AppLayout>
    );
  }

  return <AuthSignIn />;
};

export default App;
