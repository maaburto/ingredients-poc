import React, { createContext, FC, ReactNode, useState } from 'react';

export interface IAuthContext {
  isAuth: boolean;
  loggedIn: () => void;
  loggedOut: () => void
}

interface IAuthContextProvider {
  children: ReactNode | JSX.Element | JSX.Element[]
}

export const AuthContext = createContext<IAuthContext>({
  isAuth: false,
  loggedIn: () => { },
  loggedOut: () => { },
});

AuthContext.displayName = 'AuthContext';

const AuthContextProvider: FC<IAuthContextProvider> = (props) => {
  const [isAuth, setIsAuth] = useState(false);

  const loggedIn = () => {
    setIsAuth(true);
  };

  const loggedOut = () => {
    setIsAuth(false);
  };

  const contextValues = {
    isAuth,
    loggedIn,
    loggedOut,
  };

  return <AuthContext.Provider value={contextValues}>{props.children}</AuthContext.Provider>;
};

export default AuthContextProvider;
