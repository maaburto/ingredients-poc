export interface Ingredient {
  id: string;
  title: string;
  amount: number;
}
