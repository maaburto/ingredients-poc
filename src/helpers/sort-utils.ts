type SortingBasicCompareFn = (a: number | string, b: number | string, isAsc: boolean) => number;

export const sortingBasicCompare: SortingBasicCompareFn = (a, b, isAsc) => {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
};
