import React, { useContext } from 'react';
import { AuthContext } from '../../store';

import Card from '../UI/Card';
import classes from './Auth.module.css';

const AuthSignIn = () => {
  const authContext = useContext(AuthContext);

  const loginHandler = () => {
    authContext.loggedIn();
  };

  return (
    <div className={classes['auth']}>
      <Card>
        <h2>You are not authenticated!</h2>
        <p>Please log in to continue.</p>
        <button onClick={loginHandler}>Log In</button>
      </Card>
    </div>
  );
};

export default AuthSignIn;
