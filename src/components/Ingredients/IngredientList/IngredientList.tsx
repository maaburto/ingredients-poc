import { ReactNode, FC } from 'react';
import { Ingredient } from '../../../models';
import LoadingIndicator from '../../UI/LoadingIndicator';

import { IngredientItem } from '../IngredientItem';
import classes from './IngredientList.module.css';

interface IngredientListLayoutProps {
  children: ReactNode | JSX.Element | JSX.Element[];
}
const IngredientListLayout: FC<IngredientListLayoutProps> = (props) => {
  return <section className={classes['ingredient-list']}>{props.children}</section>;
};

interface IngredientListProps {
  loading: boolean;
  ingredients: Ingredient[];
  onRemoveIngredient: (id: string) => void;
}

const IngredientList: FC<IngredientListProps> = (props) => {
  if (props.loading) {
    return (
      <IngredientListLayout>
        <div className={classes['no-ingredients']}>
          <LoadingIndicator />
        </div>
      </IngredientListLayout>
    );
  }

  if (props.ingredients.length === 0) {
    return (
      <IngredientListLayout>
        <h3>{'No Ingredients, please add one'}</h3>
      </IngredientListLayout>
    );
  }

  return (
    <IngredientListLayout>
      <h2>{'Ingredients List'}</h2>
      <ul>
        {props.ingredients.map((ig) => (
          <IngredientItem
            key={ig.id}
            ingredient={ig}
            onSelectIngredient={props.onRemoveIngredient.bind(this, ig.id)}
          />
        ))}
      </ul>
    </IngredientListLayout>
  );
};

export default IngredientList;
