import React, { useEffect, useRef, useState, FC } from 'react';
import useFirstRender from '../../hooks/use-first-render';

import Card from '../UI/Card';
import './Search.css';

interface SearchProps {
  onEnteredFilterByTitle: (typeFilter: string) => void;
}

const Search: FC<SearchProps> = (props) => {
  const isFirstRender = useFirstRender();
  const { onEnteredFilterByTitle } = props;
  const [enteredFilter, setEnteredFilter] = useState('');
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    const enterFilterTimeout = setTimeout(() => {
      if (!isFirstRender && inputRef?.current?.value === enteredFilter) {
        onEnteredFilterByTitle(enteredFilter.length > 0 ? enteredFilter : 'ALL');
      }
    }, 500);

    return () => {
      clearTimeout(enterFilterTimeout);
    };
  }, [enteredFilter, onEnteredFilterByTitle, inputRef, isFirstRender]);

  return (
    <section className='search'>
      <Card>
        <div className='search-input'>
          <label>Filter by Title</label>
          <input
            ref={inputRef}
            type='text'
            value={enteredFilter}
            onChange={(event) => {
              const value = event.target.value;
              setEnteredFilter(value);
            }}
          />
        </div>
      </Card>
    </section>
  );
};

export default Search;

// const HOCSearch = (Component: FC<SearchProps>) => {
//   return (props: SearchProps) => {
//     return <Component {...props} />
//   }
// }
