import { FC } from 'react';
import { Ingredient } from '../../../models';
import classes from './ingredientItem.module.css';

interface IngredientItemProps {
  ingredient: Ingredient;
  onSelectIngredient: () => void;
}

const IngredientItem: FC<IngredientItemProps> = (props) => {
  const { ingredient } = props;

  return (
    <li className={classes['ingredient-item']} onClick={props.onSelectIngredient}>
      <span>{ingredient.title}</span>
      <span>{ingredient.amount}x</span>
    </li>
  );
};

export default IngredientItem;
