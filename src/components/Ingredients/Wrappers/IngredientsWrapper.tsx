import { useCallback, useEffect, useReducer } from 'react';
import { Ingredient } from '../../../models';
import ErrorModal from '../../UI/ErrorModal';
import { ingredientsReducer, INGREDIENTS_ACTIONS } from '../helpers';
import { useRemoveIngredients, useSortingIngredients } from '../hooks';
import IngredientFormWrapper from './IngredientFormWrapper';
import IngredientListWrapper from './IngredientListWrapper';
import SearchWrapper from './SearchWrapper';

const IngredientsWrapper = () => {
  const [allIngredients, ingredientsDispatch] = useReducer(ingredientsReducer, []);
  const {
    error: errorRemoveIngredient,
    extra: extraRemoveIngredientData,
    removeIngredient,
    clearHttpState: clearHttpRemoveIngredient,
  } = useRemoveIngredients();
  const ingredients = useSortingIngredients(allIngredients);

  useEffect(() => {
    if (
      extraRemoveIngredientData &&
      extraRemoveIngredientData.type === INGREDIENTS_ACTIONS.DELETE &&
      !errorRemoveIngredient
    ) {
      ingredientsDispatch({
        type: INGREDIENTS_ACTIONS.DELETE,
        ingredientId: extraRemoveIngredientData.data,
      });
    }
  }, [extraRemoveIngredientData, errorRemoveIngredient]);

  const removeIngredientHandler = useCallback(
    (ingredientId: string) => {
      removeIngredient(ingredientId);
    },
    [removeIngredient]
  );

  const onFetchedIngredientsHandler = useCallback((fetchedIngredients: Ingredient[]) => {
    ingredientsDispatch({
      type: INGREDIENTS_ACTIONS.SET,
      ingredients: fetchedIngredients,
    });
  }, []);

  const gettingIngredientsFilteredHandler = useCallback((filteredIngredients: Ingredient[]) => {
    ingredientsDispatch({
      type: INGREDIENTS_ACTIONS.SET,
      ingredients: filteredIngredients,
    });
  }, []);

  const newIngredientHandler = useCallback((newIngredient: Ingredient) => {
    ingredientsDispatch({
      type: INGREDIENTS_ACTIONS.ADD,
      ingredient: newIngredient,
    });
  }, []);

  return (
    <section>
      {errorRemoveIngredient && (
        <ErrorModal onClose={clearHttpRemoveIngredient}>{errorRemoveIngredient}</ErrorModal>
      )}
      <IngredientFormWrapper onSavedIngredient={newIngredientHandler} />
      <SearchWrapper onGetIngredientsFiltered={gettingIngredientsFilteredHandler} />
      <IngredientListWrapper
        ingredients={ingredients}
        onFetchedIngredients={onFetchedIngredientsHandler}
        onRemoveIngredient={removeIngredientHandler}
      />
    </section>
  );
};

export default IngredientsWrapper;
