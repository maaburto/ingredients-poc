import { FC, useCallback, useEffect } from 'react';
import { Ingredient } from '../../../models';
import ErrorModal from '../../UI/ErrorModal';
import { useNewIngredient } from '../hooks';
import { IngredientForm, IngredientFormValue } from '../IngredientsForm';

interface IngredientFormWrapperProps {
  onSavedIngredient: (newIngredient: Ingredient) => void;
}

const IngredientFormWrapper: FC<IngredientFormWrapperProps> = ({ onSavedIngredient }) => {
  const { error, clearHttpState, isLoading, addingIngredient, newIngredient } = useNewIngredient();

  useEffect(() => {
    if (newIngredient) {
      onSavedIngredient(newIngredient);
    }
  }, [newIngredient, onSavedIngredient]);

  const addIngredientHandler = useCallback(
    (ingredient: IngredientFormValue) => {
      addingIngredient(ingredient);
    },
    [addingIngredient]
  );

  return (
    <>
      {error && <ErrorModal onClose={clearHttpState}>{error}</ErrorModal>}
      <IngredientForm onAddIngredient={addIngredientHandler} loading={isLoading} />
    </>
  );
};

export default IngredientFormWrapper;
