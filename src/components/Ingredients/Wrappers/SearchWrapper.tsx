import { FC, useCallback, useEffect } from 'react';
import { Ingredient } from '../../../models';
import ErrorModal from '../../UI/ErrorModal';
import { useIngredientsFilter } from '../hooks';
import Search from '../Search';

interface SearchWrapperProps {
  onGetIngredientsFiltered: (ingredients: Ingredient[]) => void;
}

const SearchWrapper: FC<SearchWrapperProps> = ({ onGetIngredientsFiltered }) => {
  const { filteredIngredients, enteredFilterByTitle, error, clearHttpState } = useIngredientsFilter();

  useEffect(() => {
    onGetIngredientsFiltered(filteredIngredients);
  }, [filteredIngredients, onGetIngredientsFiltered]);

  const enteredFilterByTitleHandler = useCallback(
    (enteredFilter: string) => {
      const query = enteredFilter === 'ALL' ? '' : `?orderBy="title"&equalTo="${enteredFilter}"`;
      enteredFilterByTitle(query);
    },
    [enteredFilterByTitle]
  );

  return (
    <>
      {error && <ErrorModal onClose={clearHttpState}>{error}</ErrorModal>}
      <Search onEnteredFilterByTitle={enteredFilterByTitleHandler} />
    </>
  );
};

export default SearchWrapper;
