import { FC, useEffect } from 'react';
import { Ingredient } from '../../../models';
import ErrorModal from '../../UI/ErrorModal';
import { useFetchIngredients } from '../hooks';
import { IngredientList } from '../IngredientList';

interface IngredientListWrapperProps {
  ingredients: Ingredient[];
  onRemoveIngredient: (id: string) => void;
  onFetchedIngredients: (ingredients: Ingredient[]) => void;
}

const IngredientListWrapper: FC<IngredientListWrapperProps> = (props) => {
  const { onRemoveIngredient, ingredients, onFetchedIngredients } = props;
  const { ingredients: fetchedIngredients, error, isLoading, clearHttpState } = useFetchIngredients();

  useEffect(() => {
    onFetchedIngredients(fetchedIngredients);
  }, [fetchedIngredients, onFetchedIngredients]);

  return (
    <>
      {error && <ErrorModal onClose={clearHttpState}>{error}</ErrorModal>}
      <IngredientList loading={isLoading} ingredients={ingredients} onRemoveIngredient={onRemoveIngredient} />
    </>
  );
};

export default IngredientListWrapper;
