import { useMemo } from 'react';
import { sortingBasicCompare } from '../../../helpers';
import { Ingredient } from '../../../models';

const useSortingIngredients = (coreIngredients: Ingredient[], isAsc: boolean = true) => {
  const ingredients = useMemo(() => {
    return coreIngredients.sort((a, b) => sortingBasicCompare(a.title, b.title, isAsc));
  }, [coreIngredients, isAsc]);

  return ingredients;
};

export default useSortingIngredients;
