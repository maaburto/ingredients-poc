import { useCallback, useState } from 'react';
import { useHyperTextTransferProtocol } from '../../../hooks';
import { SendRequestParams } from '../../../hooks/use-http';
import { Ingredient } from '../../../models';
import { IngredientFormValue } from '../IngredientsForm';

const URL_INGREDIENTS_API = 'https://react-http-c741b-default-rtdb.firebaseio.com';

const useNewIngredient = () => {
  const [newIngredient, setNewIngredient] = useState<Ingredient>();
  const { error, isLoading, sendRequest, clear: clearHttpState } = useHyperTextTransferProtocol();

  const addingIngredient = useCallback(
    (ingredient: IngredientFormValue) => {
      const params: SendRequestParams<any, any, { name: string }> = {
        url: `${URL_INGREDIENTS_API}/ingredients.json`,
        method: 'POST',
        body: ingredient,
        onSuccess: ({ name: newId }) => {
          const newIngredient: Ingredient = {
            ...ingredient,
            id: newId,
          };
          setNewIngredient(newIngredient);
        },
      };

      sendRequest(params);
    },
    [sendRequest]
  );

  return { addingIngredient, error, isLoading, clearHttpState, newIngredient };
};

export default useNewIngredient;
