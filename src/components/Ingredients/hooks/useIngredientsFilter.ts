import { useCallback, useState } from 'react';
import useHyperTextTransferProtocol, { SendRequestParams } from '../../../hooks/use-http';
import { Ingredient } from '../../../models';
import { transformToIngredientList } from '../helpers';

const URL_INGREDIENTS_API = 'https://react-http-c741b-default-rtdb.firebaseio.com';

const useIngredientsFilter = () => {
  const [filteredIngredients, setFilteredIngredients] = useState<Ingredient[]>([]);
  const { error, isLoading, sendRequest, clear: clearHttpState } = useHyperTextTransferProtocol();

  const enteredFilterByTitle = useCallback((query: string) => {
    const params: SendRequestParams<any, any, { [key: string]: Ingredient }> = {
      url: `${URL_INGREDIENTS_API}/ingredients.json${query}`,
      method: 'GET',
      onSuccess: (dataIngredients) => {
        const loadedIngredients = transformToIngredientList(dataIngredients);
        if (loadedIngredients) {
          setFilteredIngredients(loadedIngredients);
        }
      }
    };

    sendRequest(params);
  }, [sendRequest]);


  return {
    enteredFilterByTitle,
    filteredIngredients,
    error,
    isLoading,
    clearHttpState,
  };
};

export default useIngredientsFilter;
