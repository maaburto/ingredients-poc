import { useCallback, useEffect, useState } from 'react';
import useHyperTextTransferProtocol, { SendRequestParams } from '../../../hooks/use-http';
import { Ingredient } from '../../../models';
import { transformToIngredientList } from '../helpers';

const URL_INGREDIENTS_API = 'https://react-http-c741b-default-rtdb.firebaseio.com';

const useFetchIngredients = () => {
  const [ingredients, setIngredients] = useState<Ingredient[]>([]);
  const { error, isLoading, sendRequest, clear: clearHttpState } = useHyperTextTransferProtocol();

  const fetchIngredients = useCallback(() => {
    const params: SendRequestParams<any, any, { [key: string]: Ingredient }> = {
      url: `${URL_INGREDIENTS_API}/ingredients.json`,
      method: 'GET',
      onSuccess: (dataIngredients) => {
        const loadedIngredients = transformToIngredientList(dataIngredients);
        if (loadedIngredients) {
          setIngredients(loadedIngredients);
        }
      },
    };

    sendRequest(params);
  }, [sendRequest]);

  useEffect(() => {
    fetchIngredients();
  }, [fetchIngredients]);

  return {
    ingredients,
    error,
    isLoading,
    clearHttpState,
  };
};

export default useFetchIngredients;
