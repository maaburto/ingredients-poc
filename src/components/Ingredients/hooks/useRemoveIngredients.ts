import { useCallback } from 'react';
import useHyperTextTransferProtocol from '../../../hooks/use-http';
import { INGREDIENTS_ACTIONS } from '../helpers';

const URL_INGREDIENTS_API = 'https://react-http-c741b-default-rtdb.firebaseio.com';

const useRemoveIngredients = () => {
  const { error, isLoading, sendRequest, extra, clear: clearHttpState } = useHyperTextTransferProtocol();

  const removeIngredient = useCallback(
    (ingredientId: string) => {
      sendRequest({
        url: `${URL_INGREDIENTS_API}/ingredients/${ingredientId}.json`,
        method: 'DELETE',
        extra: {
          type: INGREDIENTS_ACTIONS.DELETE,
          data: ingredientId,
        },
      });
    },
    [sendRequest]
  );

  return {
    removeIngredient,
    error,
    isLoading,
    clearHttpState,
    extra,
  };
};

export default useRemoveIngredients;
