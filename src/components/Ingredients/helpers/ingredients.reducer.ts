import { Ingredient } from '../../../models';

type IngredientState = Ingredient[];

export enum INGREDIENTS_ACTIONS {
  ADD = 'ADD_INGREDIENT',
  SET = 'SET_INGREDIENT',
  DELETE = 'DELETE_INGREDIENT',
}

type Action =
  | { type: INGREDIENTS_ACTIONS.ADD; ingredient: Ingredient }
  | { type: INGREDIENTS_ACTIONS.SET; ingredients: Ingredient[] }
  | { type: INGREDIENTS_ACTIONS.DELETE; ingredientId: string };

export type IngredientsReducerFn = (state: IngredientState, action: Action) => IngredientState;

const ingredientsReducer: IngredientsReducerFn = (ingredients, action) => {
  switch (action.type) {
    case INGREDIENTS_ACTIONS.ADD: {
      return [...ingredients, action.ingredient];
    }

    case INGREDIENTS_ACTIONS.SET: {
      return action.ingredients;
    }

    case INGREDIENTS_ACTIONS.DELETE: {
      return ingredients.filter((ingredient) => ingredient.id !== action.ingredientId);
    }

    default:
      throw new Error('Should not get there!');
  }
};

export default ingredientsReducer;
