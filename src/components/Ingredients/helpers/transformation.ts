import { Ingredient } from '../../../models';

export function transformToIngredientList(dataIngredients: any): Ingredient[] {
  if (!dataIngredients) {
    return [];
  }

  return Object.keys(dataIngredients).reduce<Ingredient[]>(
    (allItems, ingredientId) => [
      ...allItems,
      {
        id: ingredientId,
        ...dataIngredients[ingredientId],
      },
    ],
    []
  );
}
