export interface IngredientFormValue {
  title: string;
  amount: number;
}
