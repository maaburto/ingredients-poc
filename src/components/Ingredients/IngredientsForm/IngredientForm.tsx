import React, { FC, useCallback } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import LoadingIndicator from '../../UI/LoadingIndicator';
import Card from '../../UI/Card';
import classes from './IngredientForm.module.css';
import { IngredientFormValue } from './IngredientsForm.model';

interface IngredientFormProps {
  loading: boolean;
  onAddIngredient: (data: IngredientFormValue) => void;
}

const ingredientFormSchema = Yup.object().shape({
  title: Yup.string().required('Title is Required'),
  amount: Yup.number()
    .required('Amount is Required')
    .min(1)
    .when('title', {
      is: (val: string) => (val ?? '').toLowerCase() === 'apple',
      then: (schema) => schema.max(10),
    }),
});

const IngredientForm: FC<IngredientFormProps> = React.memo((props) => {
  const { onAddIngredient } = props;

  const formik = useFormik({
    initialValues: {
      title: '',
      amount: '',
    },
    validationSchema: ingredientFormSchema,
    onSubmit: (values) => {
      const data = {
        title: values.title,
        amount: parseInt(values.amount, 10),
      };
      onAddIngredient(data);

      formik.resetForm();
    },
  });

  const classesControlFormField = useCallback(
    (valueIsValid?: boolean) => `${classes['form-control']} ${valueIsValid ? classes['invalid'] : ''}`,
    []
  );

  const titleIsInvalid = !!formik?.errors?.title && !!formik?.touched?.title;
  const amountIsInValid = !!formik?.errors?.amount && !!formik?.touched?.amount;

  const titleClassesControl = classesControlFormField(titleIsInvalid);
  const amountClassesControl = classesControlFormField(amountIsInValid);

  return (
    <section className={classes['ingredient-form']}>
      <Card>
        <form onSubmit={formik.handleSubmit}>
          <div className={titleClassesControl}>
            <label htmlFor='title'>Name</label>
            <input
              type='text'
              id='title'
              value={formik.values.title}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {titleIsInvalid ? (
              <div className={classes['ingredient-form__error-text']}>{formik.errors.title}</div>
            ) : null}
          </div>
          <div className={amountClassesControl}>
            <label htmlFor='amount'>Amount</label>
            <input
              type='text'
              id='amount'
              value={formik.values.amount}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {amountIsInValid ? (
              <div className={classes['ingredient-form__error-text']}>{formik.errors.amount}</div>
            ) : null}
          </div>
          <div className={classes['ingredient-form__actions']}>
            {!props.loading && <button type='submit'>Add Ingredient</button>}
            {props.loading && <LoadingIndicator />}
          </div>
        </form>
      </Card>
    </section>
  );
});

export default IngredientForm;
