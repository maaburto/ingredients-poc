import { useEffect } from 'react';
import IngredientsWrapper from './Wrappers/IngredientsWrapper';

const Ingredients = () => {
  useEffect(() => {
    document.title = 'Ingredients';
  }, []);

  return <IngredientsWrapper />;
};

export default Ingredients;
